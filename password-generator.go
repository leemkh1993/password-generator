package main

import (
	"flag"
	"fmt"
	"math"
	"math/rand"
	"os"
	"reflect"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

var (
	message    = "Usage: password-generator.go [-h] [length]"
	components [3]string
	password   string
	hFlag      = flag.Bool("h", false, "Hash Password")
	args       []string
	length     int
)

func init() {
	// Initiate flags
	flag.Parse()
	// Get arguments, must be after parse
	args = flag.Args()
}

func main() {
	if len(args) == 0 {
		print(message)
		os.Exit(1)
	}

	// Use time as seed so it is random
	rand.Seed(time.Now().UnixNano())

	// Convert string to int
	length = intConvert(args[0])
	password = ""
	components[0] = AlphabetList()
	components[1] = IntList()
	components[2] = SymbolList()

	for len(password) < length {
		arrayIndex := index(3)
		str := components[arrayIndex]
		charIndex := index(len(str))
		// str[charIndex] = ASCII code, need to convert to string to give char
		char := string(str[charIndex])
		password += char
	}

	// Must use *, else hFlag will be the pointer
	if *hFlag {
		bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 10)
		print(string(bytes))
	} else {
		print(password)
	}
}

func AlphabetList() string {
	return "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"
}

func IntList() string {
	return "1234567890"
}

func SymbolList() string {
	return ",./<>?'[]{}|=-`~!@#$%^&*()_+"
}

func print(v interface{}) int {
	fmt.Println(v)
	return 0
}

func getType(v interface{}) string {
	return reflect.TypeOf(v).String()
}

func intConvert(str string) int {
	n, _ := strconv.ParseInt(str, 10, 0)
	// By default n = int64, need to convert to int
	return int(n)
}

func index(length int) int {
	floor := math.Floor(rand.Float64() * float64(length))
	return int(floor)
}
